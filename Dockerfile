# IMAGE PULL
FROM python:3.10

# FILE MOVE
COPY src ./src
COPY ./requirements.txt ./
COPY src/app/main.py ./

# ENVIRONMENT SETUP - FILES
WORKDIR /
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -r requirements.txt

# ENVIRONMENT SETUP - SERVER
ENV SPRING_PROFILES_ACTIVE=dev
EXPOSE 9150

# ACTIONABLE
ENTRYPOINT python main.py