# README 
## WDXIRRCalculatorMS
Service handler for testing health of a Portfolio and their handling with related operations

### Operation List
Service contains the following operations in regard to the operation for the Portfolio Health.<br> 
*This Service is referenced usually as PHS (acronym and pseudo for Portfolio Health Services)*

> _<b>Swagger UI</b>_ </br> 
> Available at:  <br>
> port: 9155 <br>
> endpoint: [/wd/phs/apidoc/swagger-ui.html](http://localhost:9155/wd/xirrc/apidoc/swagger-ui.html) <br>



IT Level Design: [Ojan Adhikari](mailto:ojan.adhikari@wealthdoor.com)<br>
Code (API-Design) and Database: [Ojan Adhikari](mailto:ojan.adhikari@wealthdoor.com)<br>
Subject-Matter Expert: [Ankit Jha](mailto:ankit.jha@wealthdoor.com)<br>
Devops: [Desh Deepak](mailto:desh.deepak@wealthdoor.com)<br>
Support(Service Design & Regulations): [Hemant Atwal](mailto:hemant.atwal@wealthdoor.com)<br>


&copy; **2023 Wealthdoor Tech Pvt. Ltd.** All Rights Reserved. </br>
<img src="https://www.wealthdoor.com/images/logo-01.png" width="135" height="40"> <br>
<a href="https://www.wealthdoor.com">Site</a> &nbsp; | &nbsp; <a href="https://www.wealthdoor.com/contact_us.html">Contact Us</a>