#!usr/bin/python3
""" Route: Master Data Operations
 Add or Modify existing Master Data vital to the operations of the tests
 provided as for by the functional user  belonging to an organization """
from __future__ import annotations

from http import HTTPStatus
from typing import Union

# IMPORTS
from fastapi import APIRouter, Header, status, Query
from fastapi.encoders import jsonable_encoder
from starlette.responses import JSONResponse

from src.app.appenum.response_code import ResponseCode
from src.app.payload.requests.generic.generic_request import GenericRequest
from src.app.payload.requests.xirr_calc_request import XirrCalcRequest
from src.app.payload.responses.generic.generic_responses import GenericResponse
from src.app.service.xirr_services.xirrc_service import XirrcService
from src.app import logs
import src.app.constants.app_constants as _consts

# CONTROL LAYER DEFINITIONS
xirrc_service_obj = XirrcService()
route_controller = APIRouter(prefix="/api/v1")


# PATH DEFINITIONS
@route_controller.post(
    path="/calculateXirr",
    tags=["XIRR Calculator "],
    summary="Calculate Values",
    status_code=200,
    response_model=GenericResponse,
)
async def calculate_xirr(
    request: GenericRequest[XirrCalcRequest],
    tenantID: Union[str, None] = Header(
        ...,
        alias=_consts.HEADER_NAME_TENANT_ID,
        title=_consts.HEADER_NAME_TENANT_ID,
        convert_underscores=False,
    ),
    orgID: Union[str, None] = Header(
        ...,
        alias=_consts.HEADER_NAME_ORG_ID,
        title=_consts.HEADER_NAME_ORG_ID,
        convert_underscores=False,
    ),
):
    """Router Function for providing data of the scenes for Back Tests"""
    # LOGS
    logs.info(f"Xirr Calculation for {orgID} under {tenantID}")

    # INITIALIZATIONS
    respond = GenericResponse

    # SERVICE-CALLS
    service_response = xirrc_service_obj.calculate_xirr(
        tenantID, orgID, request.data
    )
    
    # CASE CHECK [SUCCESS/FAILURE]
    if service_response is not None:
        respond.responseCode = ResponseCode.SUCCESS
        respond.httpStatusCode = HTTPStatus.OK
        respond.message = "XIRR Calculation Completed"
        respond.data = service_response
    else:
        respond.responseCode = ResponseCode.FAILED
        respond.message = "ERROR: Failed to Calculate"

    # RETURNS
    return JSONResponse(
        status_code=status.HTTP_200_OK,
        content=jsonable_encoder(GenericResponse(**respond.__dict__).model_dump()),
    )
