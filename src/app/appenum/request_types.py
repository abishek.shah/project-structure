#!usr/bin/python3
from enum import Enum


class RequestTypes(Enum):
    STRESS_TEST = "STRESS_TEST"
    BACKTEST = "BACKTEST"
    SCENARIO_ANALYSIS = "SCENARIO_ANALYSIS"
    BENCHMARK_ANALYSIS = "BENCHMARK_ANALYSIS"

class Frequency(str, Enum):
    ANNUALLY = "ANNUALLY"
    SEMI_ANNUALLY = "SEMI_ANNUALLY"
    MONTHLY = "MONTHLY"
    QUARTERLY = "QUARTERLY"