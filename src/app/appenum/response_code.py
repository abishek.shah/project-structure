#!usr/bin/python3
from enum import Enum


class ResponseCode(Enum):
    SUCCESS = "SUCCESS"
    FAILED = "FAILED"
