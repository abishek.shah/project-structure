#!usr/bin/python3
from starlette import status
import numpy as np
from datetime import datetime 
from scipy.optimize import fsolve
# from pyxirr import xirr

from src.app import logs

from src.app.payload.responses.xirr_calc_response import XirrCalcResponse 
from src.app.exception.generic_app_exception import GenericAppException


class XirrcService:

    def calculate_xirr(self, tenantID, orgID, data):

        dates = [d.date for d in data.xirrData]
        cashflows = [c.cashflow for c in data.xirrData]
        if len(dates) < 3:
            return None
        xirr_func = lambda r: np.sum([cf / ((1 + r) ** ((d - dates[0]).days / 365.0)) for cf, d in zip(cashflows, dates)])
        guess = 0.1
        xirr_rate = fsolve(xirr_func, guess)
        result = xirr_rate[0]
        # r = xirr(dates, cashflows)
        response = XirrCalcResponse(
            xirr=result
        )
        return response
    