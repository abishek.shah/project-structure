#!usr/bin/python3
"""
Service related constants that may not be applicable at the level of APP
"""

# PRICE GENERATION SERVICE
REFERENCE_REQUEST_ID = "reqID"
REFERENCE_CATEGORY_ID = "catID"
REFERENCE_PORTFOLIO_ID = "pfID"
REFERENCE_BENCHMARK_ID = "bmID"

# STRESS TEST SERVICE
RANGE_VALUATION_SCENE = "scenes"
RANGE_VALUATION_TIMELINE = "timeRange"
