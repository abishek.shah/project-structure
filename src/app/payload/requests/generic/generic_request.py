#!usr/bin/python3
""" Generic App Requests body """
from typing import TypeVar, Generic
from pydantic import BaseModel

# Type Definition for Generalization
T = TypeVar("T")


class GenericRequest(BaseModel, Generic[T]):
    data: T
    pageIndex: int | None = None
    pageSize: int | None = None
    searchCriteria: dict | None = None
    sortCriteria: dict | None = None
