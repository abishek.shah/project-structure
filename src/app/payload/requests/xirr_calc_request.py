#!usr/bin/python3

# IMPORTS
from pydantic import BaseModel, Field, validator
from datetime import date
from typing import List, Optional
from starlette import status

from src.app.exception.generic_app_exception import GenericAppException



class DateCashflow(BaseModel):
    date: date
    cashflow: float

class XirrCalcRequest(BaseModel):
    xirrData: List[DateCashflow]