from pydantic import BaseModel
from datetime import date
from typing import List, Dict


class XirrCalcResponse(BaseModel):
    xirr: float
