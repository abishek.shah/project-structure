#!usr/bin/python3

""" Generic Models for response """
from datetime import datetime
from http import HTTPStatus
from typing import Generic, TypeVar

from pydantic import BaseModel

from src.app.appenum.response_code import ResponseCode
from src.app.payload.responses.generic.generic_errors import GenericError

# Type Definition for Generalization
T = TypeVar("T")


class GenericResponse(Generic[T], BaseModel):
    responseCode: ResponseCode | None = None
    httpStatusCode: HTTPStatus | None = None
    message: str | None = None
    timeStamp: datetime | None = None
    errorDetails: GenericError | None = None
    data: T | None = None
    dataList: list[T] | None = []
    dropDownMap: dict | None = {}
    totalRecords: int | None = 0
    pageIndex: int | None = 0
    pageSize: int | None = 0
    totalPages: int | None = 0
