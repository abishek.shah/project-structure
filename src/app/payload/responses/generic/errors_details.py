#!usr/bin/python3
from pydantic import BaseModel


class ErrorDetails(BaseModel):
    errorCode: int = None
    errorMessage: str = None
