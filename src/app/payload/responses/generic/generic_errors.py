#!usr/bin/python3
""" Generic App Exception definitions """

from pydantic import BaseModel
from src.app.payload.responses.generic.errors_details import ErrorDetails


class GenericError(BaseModel):
    errorsList: list[ErrorDetails] = []
