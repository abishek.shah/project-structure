#!usr/bin/python3

from fastapi import FastAPI

from src.app import logs
from src.app.config.generic_config import GenericConfig
from src.app.config.server_config import Servlet
from src.app.exception.router_advisor import initiate_exception_handling

# Routes
from src.app.routes.xirr import xirr_calc_router


class XirrCalculationServiceApplication:
    app: FastAPI

    def __init__(self):
        _cfgs = GenericConfig()
        self.app = FastAPI(
            debug=True,
           title="XIRR Calculation Services",
            description="Endpoints for XIRR Calculation Services - Calculations",
            version="1.0",
            openapi_url=f"{_cfgs.get_context_root_path()}/apidoc/v3/openapi.json",
            docs_url=f"{_cfgs.get_context_root_path()}/apidoc/swagger-ui.html",
        )

    def patchRoutes(self, with_context: str):
        """return the configuration"""
        logs.info("WD XIRR Calculation Services v1.0 - STARTING")
        # Routers
        self.app.include_router(
            xirr_calc_router.route_controller, prefix=with_context
        )


if __name__ == "__main__":
    """All Operation starts from here"""
    global api
    api = XirrCalculationServiceApplication()
    initiate_exception_handling(api.app)
    servlet = Servlet()
    executor = servlet.configure_server(api)
    executor.run()
