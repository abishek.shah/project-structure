#!usr/bin/python3
import collections
from typing import Any

import pandas
from sqlalchemy import Sequence, Row


def generate_request_id(series_number: int):
    request_id = "PRQ{num:07d}".format(num=series_number)
    return request_id


def generate_id(prefix: str, counter: int):
    id_val = prefix + "{num:07d}".format(num=counter)
    return id_val


def dictFromRow(row: dict):
    """Generate Dictionary object from SQLAlchemy Row Object
    to use in Response body"""
    # INITIALIZATION
    data_row_dict = {}
    # OPERATIONS
    for item in row.__dict__:
        if not str(item).startswith("_"):
            data_row_dict[item] = row.__dict__[item]
    # RETURNS
    return data_row_dict


def frameFromSequence(dataSequence) -> pandas.DataFrame:
    """ Generate a Dataframe from SQLAlchemy Row Sequence """
    # INITIALIZATIONS
    dataList = []
    # OPERATIONS
    for dataRow in dataSequence:
        dataList.append(dictFromRow(dataRow[0]))
    # RETURNS
    return pandas.DataFrame(dataList)
