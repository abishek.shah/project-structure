#!usr/bin/python3
""" Configuration generation for reading the Application Configurations """
import yaml
import os


class GenericConfig:
    profile: str

    def __init__(self):
        # current_dir = os.path.dirname(os.path.dirname(os.getcwd()))
        config_file_path = os.path.join(
            os.path.dirname(os.path.dirname(os.path.dirname(__file__))),
            "resources",
            "config.yml",
        )
        config_file = open(config_file_path, "r")
        self.config_dict = yaml.safe_load(config_file)
        self.profile = os.environ.get("SPRING_PROFILES_ACTIVE")
        # self.profile = "dev"  # TODO:  Correct before deployment

    def get_app_name(self):
        return self.config_dict["app"]["application-name"]

    def get_connection_details(self, db_name: str):
        """Send details of the DB connections back
        as a dictionary for proper operations"""
        return {
            "driver_name": self.config_dict["app"]["datasource"][self.profile][db_name][
                "driver-name"
            ],
            "host": self.config_dict["app"]["datasource"][self.profile][db_name][
                "host"
            ],
            "port": self.config_dict["app"]["datasource"][self.profile][db_name][
                "port"
            ],
            "username": self.config_dict["app"]["datasource"][self.profile][db_name][
                "username"
            ],
            "database": self.config_dict["app"]["datasource"][self.profile][db_name][
                "database-name"
            ],
            "password": self.config_dict["app"]["datasource"][self.profile][db_name][
                "password"
            ],
        }

    def get_server_port(self):
        """Pulls the Server port from the Config file"""
        return self.config_dict["server"]["port"]

    def get_server_host(self):
        """Pulls the Server hosted URL from the Config file"""
        return self.config_dict["server"]["host"]

    def get_context_root_path(self):
        """Pulls the Server hosted URL context path from the Config file"""
        return self.config_dict["server"]["context-path"]

    def get_logging_levels(self):
        """Pulls the Logging level for Application"""
        return self.config_dict["logging"]["level"]

    def get_logging_format(self):
        """Pulls the Logging formats"""
        return self.config_dict["logging"]["format"]

    def get_logging_filename(self):
        """Pulls the filename for the Logging file"""
        return self.config_dict["logging"]["filename"]

    def get_logging_config(self):
        """Get the whole Logging Config"""
        return self.config_dict["logging"]

    def get_url_configs(self, serviceName):
        """Get Remote URL Paths"""
        return self.config_dict["wd.app"]["service"][serviceName]
