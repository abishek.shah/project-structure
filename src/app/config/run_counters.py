#!usr/bin/python3
""" Run Counters for all temporary values -- unregistered id setups
"""
# Unregistered Portfolio
global unreg_pf_counter

# Unregistered Benchmark
global unreg_bm_counter

# PRESET VALUES
unreg_pf_counter = 0
unreg_bm_counter = 0


def getUnregisteredPfCount() -> int:
    """Send back the next ID on which the
    portfolioID is to be created
    """
    return unreg_pf_counter + 1


def getUnregisteredBmCount() -> int:
    """Send back the next ID on which the
    benchmarkID is to be created
    """
    return unreg_bm_counter + 1
