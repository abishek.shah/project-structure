#!usr/bin/python3
""" GENERIC LOGGING CONFIG FOR ALL SERVICES """
import logging
from src.app.config.generic_config import GenericConfig


class ServiceLogger:
    log_definition: dict
    log_levels: dict
    log_filename: str
    log_formats: str
    logger: logging.Logger

    def __init__(self):
        _get_Config = GenericConfig()
        self.log_definition = _get_Config.get_logging_config()
        self.log_filename = _get_Config.get_logging_filename()
        self.log_levels = _get_Config.get_logging_levels()
        self.log_formats = _get_Config.get_logging_format()
        self.logger = logging.getLogger(_get_Config.get_app_name())

    def logging_setup_console(self):
        """SETS UP LOGGING FOR THE CONSOLE"""
        # TODO: REMOVE DUAL INITIATION
        fmt = logging.Formatter(self.log_formats)
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)
        ch.setFormatter(fmt)
        self.logger.addHandler(ch)

    def logging_setup_file(self):
        fmt = logging.Formatter(self.log_formats)
        ch = logging.FileHandler(self.log_filename, mode="a")
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(fmt)
        self.logger.addHandler(ch)

    def set_logger(self):
        self.logging_setup_console()
        self.logging_setup_file()
        self.logger.setLevel(logging.DEBUG)
        print(self.logger)
        return self.logger


def getServiceLogger():
    """Returns Service Logger"""
    log_obj = ServiceLogger()
    logs = log_obj.set_logger()
    return logs
