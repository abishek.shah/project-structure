#!usr/bin/python3

from sqlalchemy.orm import Session
from sqlalchemy import select, insert, delete
from sqlalchemy.engine import Engine
from src.app.config.generic_config import GenericConfig
from src.app.database.data_connector import DBConnection
from src.app.database.entity.market_data import MarketData
from src.app import logs
from src.app.constants import app_constants as _consts


class MarketDataRepository:
    """Repository for accessing information to and from Database
    Table: MarketData
    """

    # Class Variables
    _session: Session
    _engine: Engine

    def __init__(self):
        # PULL CONFIGURATIONS
        configs = GenericConfig()
        config_details = configs.get_connection_details(_consts.MARKET_DATA_DATABASE)
        self._session, self._engine = DBConnection.get_db_session_creator(
            **config_details
        )

    def get_market_ticker_data(self, tnt_id: str, org_id: str, ticker: str):
        try:
            get_query = (
                select(MarketData)
                .where(MarketData.tntID == tnt_id)
                .where(MarketData.orgID == org_id)
                .where(MarketData.ticker == ticker)
            )
            results = self._session.execute(get_query).fetchall()
            logs.info(f"SqlAlchemy:{get_query}")
            return results
        except Exception as err:
            self._session.rollback()
            logs.error(f" DB Error: {err.__cause__}")

    def insert_market_data(self, tnt_id: str, org_id: str, value_map: dict):
        try:
            insert_query = insert(MarketData).values(**value_map).returning()
            result = self._session.execute(insert_query)
            logs.info(f"SqlAlchemy:{insert_query}")
            return result
        except Exception as err:
            self._session.rollback()
            logs.error(f" DB Error: {err.__cause__}")

    def flush_market_data(self, tnt_id: str, org_id: str, date: str):
        try:
            delete_query = delete(MarketData).where(MarketData.date == date).returning()
            result = self._session.execute(delete_query)
            logs.info(f"SqlAlchemy:{delete_query}")
            return result
        except Exception as err:
            self._session.rollback()
            logs.error(f" DB Error: {err.__cause__}")
