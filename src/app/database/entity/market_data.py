#!usr/bin/python3

# IMPORTS
from sqlalchemy import Column
from sqlalchemy import String, BIGINT, TIMESTAMP, FLOAT
from sqlalchemy.ext.declarative import declarative_base

# -- SQL SCRIPTS
"""
    -- MARKET DATA
CREATE TABLE IF NOT EXISTS wdphsschema.MARKET_DATA(
    mktd_seqid          bigint NOT NULL DEFAULT nextval('wdphsschema.seq_mkt_data'::regclass),
    mktd_tntid          varchar(10),
    mktd_orgid          varchar(10),
    mktd_date           timestamp,
    mktd_isin           varchar(12),
    mktd_ticker         varchar(20),
    mktd_name           varchar(50),
    mktd_open           float8,
    mktd_high           float8,
    mktd_low            float8,
    mktd_close          float8,
    mktd_volume         float8,
    mktd_adj_close      float8,
    created_userid      varchar(50),
    created_date        timestamp,
    modified_userid     varchar(50),
    modified_date       timestamp,
    CONSTRAINT mkt_dta_pk PRIMARY KEY (mktd_seqid)
);
"""


class MarketData(declarative_base()):
    __tablename__ = "MARKET_DATA"
    __table_args__ = {"quote": False, "extend_existing": True, "schema": "wdphsschema"}
    seqID = Column(
        name="mktd_seqid", type_=BIGINT, primary_key=True, autoincrement="auto"
    )
    tntID = Column(name="mktd_tntid", type_=String)
    orgID = Column(name="mktd_orgid", type_=String)
    date = Column(name="mktd_date", type_=TIMESTAMP)
    isin = Column(name="mktd_isin", type_=String)
    ticker = Column(name="mktd_ticker", type_=String)
    name = Column(name="mktd_name", type_=String)
    openVal = Column(name="mktd_open", type_=FLOAT)
    highVal = Column(name="mktd_high", type_=FLOAT)
    lowVal = Column(name="mktd_low", type_=FLOAT)
    closeVal = Column(name="mktd_close", type_=FLOAT)
    volumeVal = Column(name="mktd_volume", type_=FLOAT)
    adjCloseVal = Column(name="mktd_adj_close", type_=FLOAT)
    createdUserID = Column(name="created_userid", type_=String)
    createdDate = Column(name="created_date", type_=TIMESTAMP)
    modifiedUserID = Column(name="modified_userid", type_=String)
    modifiedDate = Column(name="modified_date", type_=TIMESTAMP)
