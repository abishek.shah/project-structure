#!usr/bin/python3
""" Connection to wire in with the database
and access the Database services """

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import exc
from sqlalchemy.orm import sessionmaker

from src.app import logs


class DBConnection:
    @staticmethod
    def get_db_session_creator(driver_name, username, password, host, port, database):
        """Create the DB with the proper connection details"""
        session_object = None
        engine = None
        try:
            engine = create_engine(
                sqlalchemy.engine.URL.create(
                    drivername=driver_name,
                    username=username,
                    password=password,
                    host=host,
                    port=port,
                    database=database,
                )
            )
        except exc.SQLAlchemyError as err:
            logs.error("unable to establish engine connection-{}".format(err))
        try:
            session_local = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session_object = session_local()
        except exc.SQLAlchemyError as e:
            logs.error(40, "unable to establish session-{}".format(e))

        # Return connections
        return session_object, engine
